package eu.dreamix.thread;

public class Main {

    public static void main(String[] args) {
        int numberOfRuns = 1000;
        long t0[], t1[],
                sum1, sum2,
                t2[], t3[];

        t0 = new long[numberOfRuns];
        t1 = new long[numberOfRuns];

        sum1 = 0;

        for (int i = 0; i < numberOfRuns; i++) {

            t0[i] = System.nanoTime();
            Thread thread = new ThreadChild();
            thread.start();
            try {
                thread.join();  // wait for the thread to finish
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            t1[i] = System.nanoTime();

            sum1 += t1[i] - t0[i];
        }

        System.out.println("\nThread average: " + sum1 / numberOfRuns + " ns");

        t2 = new long[numberOfRuns];
        t3 = new long[numberOfRuns];

        sum2 = 0;

        for (int i = 0; i < numberOfRuns; i++) {

            t2[i] = System.nanoTime();
            Runnable runnable = new RunnableImplementation();
            Thread threadForRunnable = new Thread(runnable);
            threadForRunnable.start();
            try {
                threadForRunnable.join();  // wait for the thread to finish
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            t3[i] = System.nanoTime();

            sum2 += t3[i] - t2[i];
        }
        System.out.println("\nRunnable average: " + sum2 / numberOfRuns + " ns");

        double ratio = (double) (sum2 / numberOfRuns) / (sum1 / numberOfRuns);
        System.out.println(ratio);

    }


}
